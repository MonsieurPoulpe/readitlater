#!/usr/bin/env bash

echo "+---------------------------------------------------------+"
echo "|                                                         |"

if [ $1 == "devapp" ] ; then
    echo "|               Provisioning web Sandbox                  |"
elif [ $1 == "db" ] ; then
    echo "|               Provisioning db Sandbox                   |"
fi

echo "|                                                         |"
echo "+---------------------------------------------------------+"

echo "Updating apt mirrors..."
(
    md5check=`echo "97fe9dd10455fd6e869c6c3e04a9db89  /etc/apt/sources.list" | md5sum --status -c -`
    if [ $? -ne 0 ] ; then
        cp -f /vagrant/bootstrap/apt/sources.list /etc/apt/sources.list
        chmod 0644 /etc/apt/sources.list
        apt-get update -qq
    fi
)

if [ $1 == "devapp" ] ; then
    echo "Checking pip..."
    ispip= `which pip`
    if [ $? -ne 0 ] ; then
        echo "Installing pip3 and python 3.4..."
        (
            apt-get install -qq -y python3-pip python3.4
        )
        echo "Installing Django..."
        (
            pip3 install django
        )
        echo "Installing Django OAuth Toolkit..."
        (
            pip3 install django-oauth-toolkit
        )
        echo "Installing Celery..."
        (
            pip3 install celery
        )
    fi
elif [ $1 == "db" ] ; then
    /usr/bin/env python3 -u /vagrant/bootstrap/$1.py
fi
